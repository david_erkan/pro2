
package part08;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {

    
    public static void main(String[] args) {
        
//        for( int i : new Range (10))
//        System .out. print (i+" "); // affiche 0 1 2 3 4 5 6 7 8 9
//        System.out.println("");
//        
//        for( int i : new Range ( -5 ,5))
//        System .out. print (i+" "); // affiche -5 -4 -3 -2 -1 0 1 2 3 4
//        System.out.println("");
//        
//        for( int i : new Range (0 ,10 ,2))
//        System .out. print (i+" "); // affiche 0 2 4 6 8
//        System.out.println("");
//        
//        for( int i : new Range (10 ,0 , -1))
//        System .out. print (i+" "); // affiche 10 9 8 7 6 5 4 3 2 1

        Powers p;
        try {
            p = new Powers (2 ,1000); // puissances de 2 <= 1000
            for( int i : p)
            System .out. println (i);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
