
package part08;

import java.util.Iterator;

public class Range implements Iterable<Integer>{
    int min;
    int max;
    int step;

    public Range(int min, int max, int step) {
        this.min = min;
        this.max = max;
        this.step = step;
    }

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
        this.step = 1;
    }

    public Range(int max) {
        this.max = max;
        this.min = 0;
        this.step = 1;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            
            int current = min;
            
            @Override
            public boolean hasNext() {
                return current < max;
            }

            @Override
            public Integer next() {
                int copy = current;
                current += step;
                return copy;
            }
        };
    }
    
    
}
