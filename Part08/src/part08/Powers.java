
package part08;

import java.util.Iterator;

public class Powers implements Iterable<Integer>{
    
    int powersOf;
    int limit;

    public Powers(int powersOf, int limit) throws Exception {
        if (powersOf < 2)
            throw new Exception("Can't accept value");
        this.powersOf = powersOf;
        this.limit = limit;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            
            int current = 1;
            
            @Override
            public boolean hasNext() {
                return current <= limit;
            }

            @Override
            public Integer next() {
                int copy = current;
                current *= powersOf;
                return copy;
            }
        };
    }
    
    
}
