
package part08;

public interface Movable {
    public void move(Direction d);
}
