
package part08;

public class Point implements Movable{
    
    int x;
    int y;
    
    @Override
    public void move(Direction d) {
        switch(d){
            case NORD : --y; break;
            case EST : ++x; break;
            case SUD : ++y; break;
            case OUEST : --x; break;
        }
    }
    
}
