
package myarraylist;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Template;
import java.util.List;
import javax.print.DocFlavor;

public class Main {
    public static void main(String[] args) {
        List<String> l;
        l = new MyArrayList<>();
        l.add("a");
        l.add("b");
        l.add("c");
        
        affiche(l);
        
        l.set(0, "d");
        l.set(1, "e");
        l.set(2, "f");
        l.add("g");
        
        affiche(l);
        
        l.remove("g");
        l.remove(0);
        
        affiche(l);
        
        l.add("g");
        l.add(0, "d");
        
        affiche(l);
        System.out.println(l.size());
        
        l.add(3, "i");
        l.add(3, "h");
        
        affiche(l);
        
        l.add(l.size() - 1, "k");
        l.set(l.size() - 2, "j");
        l.remove(l.size() - 1);
        
        affiche(l);
    }
    
    
    static void affiche(List l){
        System.out.println("");
        for (Object o : l) {
            System.out.println(o);
        }
    }
}
