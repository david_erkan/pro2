
package myarraylist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class Node<E> {
    E item;
    Node<E> prev;
    Node<E> next;

    public Node() {
        next = this;
        prev = this;
    }

    public Node(E item) {
        this.item = item;
    }
}

public class MyArrayList<E> implements List<E>{
    
    private Node<E> head;
    private int size;

    public MyArrayList() {
        size = 0;
        head = new Node<E>();
    }
    
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return find( (E) o) != null;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            
            Node<E> head = MyArrayList.this.head;
            
            @Override
            public boolean hasNext() {
                return head.next != null && head.next != MyArrayList.this.head;
            }

            @Override
            public E next() {
                head = head.next;
                return head.item;
            }
        };
    }
    
    Iterator<Node<E>> nodeIterator(){
        return new Iterator<Node<E>>() {
            
            Node<E> head = MyArrayList.this.head;
            
            @Override
            public boolean hasNext() {
                return head.next != null && head.next != MyArrayList.this.head;
            }

            @Override
            public Node<E> next() {
                return head = head.next;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        Iterator<E> it = iterator();
        for (int i = 0; i < size - 1; i++) {
            array[i] = it.next();
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(E e) {
        addAfter(e, head.prev);
        return true;
    }
    
    private void addAfter(E e, Node<E> after){
        Node toAdd = new Node(e);
        toAdd.prev = after;
        toAdd.next = after.next;
        after.next = toAdd;
        toAdd.next.prev = toAdd;
        ++size;
    }

    @Override
    public boolean remove(Object o) {
        Node n = find( (E) o );
        if (n != null){
            removeNode(n);
            return true;
        }
        return false;
    }
    
    private void removeNode(Node<E> toRemove){
        Node prev = toRemove.prev;
        Node next = toRemove.next;
        prev.next = next;
        next.prev = prev;
        toRemove = null;
        --size;
    }
    
    private Node<E> find(E e){
        Node<E> result = head;
        while(result.next != null && size > 0){
            result = result.next;
            if (result == head) break;
            if (result.item.equals(e) ) return result;
        }
        
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null) throw new NullPointerException();
        if (c.isEmpty() ) return false;
        for (Object o : c) {
            if (!contains(o) )
                return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (c == null) throw new NullPointerException();
        if (c.isEmpty() ) 
            return false;
        for(E e : c)
            add(e);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if (c == null) throw new NullPointerException();
        if (c.isEmpty() ) return false;
        Node<E> toAddAfter = getNode(index);
        for (E elem : c){
            addAfter(elem, toAddAfter);
            toAddAfter = toAddAfter.next;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null) throw new NullPointerException();
        if (c.isEmpty() ) return false;
        for (Object elem : c) {
            Node<E> n = find((E) elem);
            if (n == null) continue;
            remove(n.item);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        if (c == null) throw new NullPointerException();
        if (c.isEmpty() ) return false;
        for (E e : this){
            if (!c.contains(e) )
                remove(e);
        }
        return true;
    }

    @Override
    public void clear() {
        if (size > 0)
        head.next = head;
        head.prev = head;
    }

    @Override
    public E get(int index) {
        return getNode(index).item;
    }
    
    Node<E> getNode(int index){
        if (index > size - 1) throw new IndexOutOfBoundsException(Integer.toString(index));
        Node<E> result = head;
        for (int i = 0; i < size; i++) {
            result = result.next;
            if (i == index) return result;
        }
        return null;
    }

    @Override
    public E set(int index, E element) {
        Node<E> n = getNode(index);
        n.item = element;
        return n.item;
    }

    @Override
    public void add(int index, E element) {
        addAfter(element, getNode(index).prev);
    }

    @Override
    public E remove(int index) {
        Node<E> n = getNode(index);
        removeNode(n);
        return n.item;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        for (E elem : this){
            if (elem.equals(o)){
                ++index;
                break;
            }
        }
        return index;
    }
    
    Node<E> getIndexOfNode(E e){
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
